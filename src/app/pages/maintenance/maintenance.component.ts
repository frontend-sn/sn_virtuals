import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConstantService} from 'src/app/services/constant.service';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.scss', '../shared.scss']
})

export class MaintenanceComponent implements OnInit {

  cycleTimer: any = '';

  timeLeft: number = 60;
  interval: any;

  $: any;

  /* #region  mute un mute */
  newCounterLength: number = 60;
  newCounterInterval: any;


  constructor(
    public constant: ConstantService,
    public router: Router
  ) {
  }

  ngOnInit(): any {

  }

}
