import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConstantService} from 'src/app/services/constant.service';

@Component({
  selector: 'app-andarbahar',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '../shared.scss']
})

export class AndarbaharComponent implements OnInit {
  state = 'new';
  selected = 'false';

  cycleTimer: any = '';

  isBatPlacedOpen: boolean = false;
  isWinnerDecided: boolean = false;
  isBetDialerClick: boolean = false;

  // @Input() betState = null;

  GameWinner = 'JOKER';

  player = 'Bet for';
  odds = null;

  timeLeft: number = 60;
  interval: any;

  $: any;

  /* #region  mute un mute */
  newCounterLength: number = 60;
  newCounterInterval: any;
  public audio: HTMLAudioElement;
  isMute: boolean = true;

  /* #endregion mute un mute */

  startTimer() {
    let count = 0;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        count = count + 4.18;
        document.getElementById('progress').setAttribute('stroke-dasharray', count + ', 251.2');
      } else {
        this.selected = 'false';
        this.state = 'new';
        const buttons = document.querySelectorAll('.button');
        buttons.forEach(el => el.classList.remove('active'));
        this.timeLeft = 60;
      }

      // Sequence
      if (this.timeLeft <= 60) {
        this.state = 'new';
        document.getElementById('content').classList.remove('winner');
      }
      if (this.timeLeft <= 50) {
        this.state = 'bet';
        document.getElementById('content').classList.remove('winner');
      }
      if (this.timeLeft <= 20) {
        this.state = 'open';
        document.getElementById('content').classList.add('winner');
      }
    }, 1000);
  }

  constructor(
    public constant: ConstantService,
    public router: Router
  ) {
  }

  GameList: any;
  CurrentGame: string;
  GameCode: string;
  GameStatus: number = 1;

  quickBet = false;

  receiveMessage($event) {
    this.quickBet = $event;
  }

  AddBet(player, odds) {
    if (this.GameStatus === 1) {
      if (this.quickBet) {
        this.player = player;
        this.odds = odds;
        new window.bootstrap.Toast(document.getElementById('success')).show();
      } else {
        this.player = player;
        this.odds = odds;
      }
    } else {
      // this.toastr.error('Betting closed. You can\'t bet right now.');
    }
  }

  toasts() {
    var x = document.getElementById('snackbar');
    x.className = 'show';
    setTimeout(function() {
      x.className = x.className.replace('show', '');
    }, 5000);
  }

  public muteAudio(isMute: any): void {
    this.isMute = isMute;
  }

  ngOnInit() {
    /* #region sound mute unmute  */
    this.audio = new Audio();
    this.audio.src = '../../../assets/audio/beep.mp3';
    this.audio.load();
    // this.audio.autoplay = true;
    this.audio.play();

    this.GameList = this.constant.gameList;
    this.GameCode = this.router.url.replace('/', '');
    this.CurrentGame = this.GameList[this.GameCode].name;

    this.startTimer();

    const buttons = document.querySelectorAll('.button');

    buttons.forEach(el => el.addEventListener('click', event => {
      buttons.forEach(el => el.classList.remove('active'));
      el.classList.add('active');
      this.selected = 'true';
    }));

    this.createTimer(this.newCounterLength);
  }

  createTimer(time) {
    var countrObj: any = {};
    countrObj.counter = document.getElementById('cycleTimer');
    countrObj.counter = countrObj.counter.getContext('2d');
    countrObj.no = time;
    countrObj.pointToFill = 4.72;
    countrObj.cw = countrObj.counter.canvas.width;
    countrObj.ch = countrObj.counter.canvas.height;
    countrObj.diff;

    var fill = setInterval(() => {
      if (this.newCounterLength != 0) {
        this.newCounterLength = this.newCounterLength - 1;
      }
      this.fillCounter(countrObj, time, fill);
      if (this.isMute) {
        this.audio.pause();
      } else {
        this.audio.play();
      }
    }, 1000);
  }

  fillCounter(countrObj, time, fill) {
    countrObj.diff = ((countrObj.no / time) * Math.PI * 2 * 10);
    countrObj.counter.clearRect(0, 0, countrObj.cw, countrObj.ch);
    countrObj.counter.lineWidth = 4;
    countrObj.counter.strokeStyle = '#E800EB';
    countrObj.counter.beginPath();
    countrObj.counter.arc(110, 110, 70, countrObj.pointToFill, countrObj.diff / 10 + countrObj.pointToFill);
    countrObj.counter.stroke();

    if (countrObj.no == 0) {
      clearTimeout(fill);
    }
    countrObj.no--;
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  toggleIsBatPlacedOpen(): void {
    this.isBetDialerClick = false;
    this.isBatPlacedOpen = true;
    setTimeout(() => {
      const batPlaced = document.getElementsByClassName('doranim');
      batPlaced[0].className += ' ' + 'opendoor';
    }, 0);

    setTimeout(() => {
      const batPlaced = document.getElementsByClassName('doranim');
      batPlaced[0].className = batPlaced[0].className.replace('opendoor', '');
    }, 3000);

    setTimeout(() => {
      this.isBatPlacedOpen = false;
      this.isWinnerDecided = true;

      const andarSecondSectionCss = document.getElementsByClassName('andar-second-section')[0];
      const andarSecondSectionCssMob = document.getElementsByClassName('andar-second-sectionMob')[0];
      if (andarSecondSectionCss) {
        andarSecondSectionCss.className += ' ' + 'win';

        setTimeout(() => {
          this.isWinnerDecided = false;
          andarSecondSectionCss.className = andarSecondSectionCss.className.replace('win', '');
          this.resetAllVariablesOfBet();
        }, 3000);
      }
      if (andarSecondSectionCssMob) {
        andarSecondSectionCssMob.className += ' ' + 'win';

        setTimeout(() => {
          this.isWinnerDecided = false;
          andarSecondSectionCssMob.className = andarSecondSectionCssMob.className.replace('win', '');
          this.resetAllVariablesOfBet();
        }, 3000);
      }
    }, 4000);
  }

  resetAllVariablesOfBet(): any {
    this.isBatPlacedOpen = false;
    this.isWinnerDecided = false;
  }
}
