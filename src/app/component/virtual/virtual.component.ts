import { Component, OnInit } from '@angular/core';
declare const cards: any;
declare const STANDARD: any;

@Component({
	selector: 'app-virtual',
	templateUrl: './virtual.component.html',
	styleUrls: ['./virtual.component.scss']
})

export class VirtualComponent implements OnInit {

	animation = 'breathe';
	GameCode = '';
	GameHistoryDetails: any = { PlayerACard: [], PlayerBCard: [], PlayerCCard: [], PlayerDCard: [] };
	clickCount = 0;

	dynamicClassName = "breathe"

	setAnimation(animation) {
		this.animation = animation;
		setTimeout(() => {
			this.animation = 'breathe';
		}, 9000);
	}

	constructor() { }

	ngOnInit(): void {
		let path = location.pathname;
		path = path.slice(1);
		this.GameCode = path.trim();

		setTimeout(() => {
			this.dynamicClassName = "A2"

			setTimeout(() => {
				this.dynamicClassName = "A3"

				setTimeout(() => {
					this.dynamicClassName = "A4"
					// IN
					$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").addClass('pos');
					$(".cardPerspective").addClass("cardsShuffleing");

					animateDiv('.ccard0');
					animateDiv('.lcard0');
					animateDiv('.lcard1');
					animateDiv('.lcard2');
					animateDiv('.lcard3');
					animateDiv('.lcard4');
					animateDiv('.lcard5');
					animateDiv('.lcard6');
					animateDiv('.lcard7');
					animateDiv('.lcard8');
					animateDiv('.lcard9');
					animateDiv('.rcard0');
					animateDiv('.rcard1');
					animateDiv('.rcard2');
					animateDiv('.rcard3');
					animateDiv('.rcard4');
					animateDiv('.rcard5');
					animateDiv('.rcard6');
					animateDiv('.rcard7');
					animateDiv('.rcard8');
					animateDiv('.rcard9');

					function makeNewPosition(){
						var h = 50;
						var w = 200;
						var nh = Math.floor(Math.random() * h);
						var nw = Math.floor(Math.random() * w);
						return [nh,nw];
					}
					
					function animateDiv(myclass){
						var newq = makeNewPosition();
						$(myclass).addClass("pos");
						$(myclass).animate({ 
							top: newq[0],
							left: newq[1],
						}, 900,
						function(){
							animateDiv(myclass);
						});
					};
					
					setTimeout(() => {
						
						$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").stop();
						$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeClass("pos");
						$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeAttr('style');
						
						setTimeout(() => {
							$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").addClass("transformRot");
							setTimeout(() => {
								$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").addClass("transformTranslate");
							}, 4000);
							setTimeout(() => {
								$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeClass("transformTranslate");
								$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeClass("transformRot");
								$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeClass("transformRot");
								$(".cardPerspective").removeClass("cardsShuffleing");
							}, 7500);
						}, 1000);
					}, 3500);

					setTimeout(() => {
						this.dynamicClassName = "A5"

						setTimeout(() => {
							this.dynamicClassName = "A6"				

							setTimeout(() => {
								this.dynamicClassName = "A1"
					
							}, 5000);

						}, 8000);
			
					}, 5000);
		
				}, 15000);

			}, 8000);

		}, 2000);

	}

	SetHistoryCards() {
		this.setAnimation('breathe');
		this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// this.clickCount += 1;
		// if (this.GameCode === 'TP') {
		// 	if (this.clickCount % 2 == 0) {
		// 		this.GameHistoryDetails.PlayerBCard.push('assets/images/card.png');
		// 	} else {
		// 		this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// 	}
		// } else if (this.GameCode === 'WM') {
		// 	this.GameHistoryDetails.PlayerBCard.push('assets/images/card.png');
		// } else if (this.GameCode === 'DT') {
		// 	this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// } else if (this.GameCode === 'PK' || this.GameCode === 'AB') {
		// 	this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// } else if (this.GameCode === 'BC' || this.GameCode === 'RPS') {
		// 	this.GameHistoryDetails.PlayerBCard.push('assets/images/card.png');
		// }
		// // mycode 
		// else if (this.GameCode === 'LS' || this.GameCode === 'JK' || this.GameCode === 'ARW') {
		// 	this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// } else if (this.GameCode === 'CQ' || this.GameCode === 'TTC' || this.GameCode === 'KR') {
		// 	if (this.clickCount === 1 || this.clickCount === 5 || this.clickCount === 9 || this.clickCount === 13 || this.clickCount === 17 || this.clickCount === 21) {
		// 		this.GameHistoryDetails.PlayerACard.push('assets/images/card.png');
		// 	} if (this.clickCount === 2 || this.clickCount === 6 || this.clickCount === 10 || this.clickCount === 14 || this.clickCount === 18 || this.clickCount === 22) {
		// 		this.GameHistoryDetails.PlayerBCard.push('assets/images/card.png');
		// 	} if (this.clickCount === 3 || this.clickCount === 7 || this.clickCount === 11 || this.clickCount === 15 || this.clickCount === 19 || this.clickCount === 23) {
		// 		this.GameHistoryDetails.PlayerCCard.push('assets/images/card.png');
		// 	} if (this.clickCount === 4 || this.clickCount === 8 || this.clickCount === 12 || this.clickCount === 16 || this.clickCount === 20 || this.clickCount === 24) {
		// 		this.GameHistoryDetails.PlayerDCard.push('assets/images/card.png');
		// 	}
		// }
		this.setAnimation('gesture-A');
	}
}

	// ngOnInit(): void {
	// 	let path = location.pathname;
	// 	path = path.slice(1);
	// 	this.GameCode = path.trim();

	// 	cards.init({ table: '#card-table', type: STANDARD });
	// 	let deck = new cards.Deck();
	// 	deck.x = 95;
	// 	deck.y = 65;

	// 	deck.addCards(cards.all);
	// 	deck.render();

	// 	let andar = new cards.Hand({ faceUp: true, x: 25, y: 65 });
	// 	let bahar = new cards.Hand({ faceUp: true, x: 25, y: 80 });

	// 	let n = 0;
	// 	deck.click((card) => {
	// 		this.setAnimation('gesture-A');
	// 		if (card === deck.topCard()) {
	// 			n += 1;
	// 			if (n % 2 == 0) {
	// 				andar.addCard(deck.topCard());
	// 				andar.render({ image: './assets/images/card.png' });
	// 			} else {
	// 				bahar.addCard(deck.topCard());
	// 				bahar.render({ image: './assets/images/card.png' });
	// 			}
	// 		}
	// 	});
	// }


// Cards Shaflunig

// $(document).ready(function(){
// 	animateDiv('.ccard0');
// 	animateDiv('.lcard0');
// 	animateDiv('.lcard1');
// 	animateDiv('.lcard2');
// 	animateDiv('.lcard3');
// 	animateDiv('.lcard4');
// 	animateDiv('.lcard5');
// 	animateDiv('.lcard6');
// 	animateDiv('.lcard7');
// 	animateDiv('.lcard8');
// 	animateDiv('.lcard9');
// 	animateDiv('.rcard0');
// 	animateDiv('.rcard1');
// 	animateDiv('.rcard2');
// 	animateDiv('.rcard3');
// 	animateDiv('.rcard4');
// 	animateDiv('.rcard5');
// 	animateDiv('.rcard6');
// 	animateDiv('.rcard7');
// 	animateDiv('.rcard8');
// 	animateDiv('.rcard9');
// });

// function makeNewPosition(){
    
// 	// Get viewport dimensions (remove the dimension of the div)
// 	// var h = $(window).height() - 50;
// 	// var w = $(window).width() - 50;
// 	var h = 50;
// 	var w = 200;
	
// 	var nh = Math.floor(Math.random() * h);
// 	var nw = Math.floor(Math.random() * w);
	
// 	return [nh,nw];
    
// }

// function animateDiv(myclass){
// 	var newq = makeNewPosition();
//   $(myclass).addClass("pos");
// 	$(myclass).animate({ 
// 		top: newq[0],
// 		// transform: 'rotate(' + newq[0] + 'deg)',
// 		// transform: newq[0],
// 		left: newq[1],
// 	}, 900,
// 	function(){
// 		animateDiv(myclass);
// 	});
// };


// setTimeout(() => {`
	
// 	$(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").stop();
//   $(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeClass("pos");
//   $(".ccard0, .lcard0, .lcard1, .lcard2, .lcard3, .lcard4, .lcard5, .lcard6, .lcard7, .lcard8, .lcard9, .rcard0, .rcard1, .rcard2, .rcard3, .rcard4, .rcard5, .rcard6, .rcard7, .rcard8, .rcard9").removeAttr('style');
	
// 	setTimeout(() => {
//   	$(".cardsShuffleing").removeClass("cardsShuffleing");
// 	}, 1000);
// }, 3500);`