import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
import { Subscription } from 'rxjs';
import $ from "jquery";

@Component({
  selector: 'app-bet-dial',
  templateUrl: './bet-dial.component.html',
  styleUrls: ['./bet-dial.component.scss'],
})
export class BetDialComponent implements OnInit {

  @Input() selected: any;
  @Input() betState: any;
  @Input() state: any;

  htmlCollection: any = '';
  itemsId: any = '';
  sectionDeg: any = '';
  radianSectionDeg: any = '';
  radiusLength: any = 200;
  rotation: any = 0;
  center: any = '';
  interval: any;

  quickBet = false;

  isBatPlacedOpen: boolean = false;
  isWinnerDecided: boolean = false;
  isBetDialerClick: boolean = false;

  stake: number;
  stakeSubscription: Subscription;

  constructor(private data: ConstantService) {}

  @Output() messageEvent = new EventEmitter<boolean>();
  // @Output() onBetPlacedEvent = new EventEmitter<string>();
  @Output('onBetPlacedEvent') onBetPlacedEvent: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {

    setTimeout(() => {
      this.htmlCollection = document.getElementsByClassName('item') as HTMLCollectionOf<HTMLElement>;
      this.itemsId = Array.from(this.htmlCollection);
      // console.log(this.itemsId);

      this.sectionDeg = 360 / this.itemsId.length;
      this.radianSectionDeg = this.sectionDeg * Math.PI * 2 / 360;
      this.radiusLength = 165;
      for (var i = 0; i < this.itemsId.length; i++) {
        this.itemsId[i].style.top = this.radiusLength * Math.sin(this.radianSectionDeg * i - 1.5708) - 50 + 'px';
        this.itemsId[i].style.left = this.radiusLength * Math.cos(this.radianSectionDeg * i - 1.5708) - 50 + 'px';
      }
      this.rotation = 0;
      this.center = document.getElementById('center');

      this.turnRight();
      this.turnLeft();
    }, 500);
    
  }

  onBetPlaced(): void {
    this.onBetPlacedEvent.emit();
  }
  
  // Slider JS
  turnLeft() {
    this.rotation = this.rotation + this.radianSectionDeg;
    this.center.style.transform = 'rotate(' + this.rotation + 'rad) scale(0.79)';
    for (var i = 0; i < this.itemsId.length; i++) {
      this.itemsId[i].style.transform = 'rotate(' + -this.rotation + 'rad) scale(0.79)';
    }
  }

  turnRight() {
    this.rotation = this.rotation - this.radianSectionDeg;
    this.center.style.transform = 'rotate(' + this.rotation + 'rad) scale(0.79)';
    for (var i = 0; i < this.itemsId.length; i++) {
      this.itemsId[i].style.transform = 'rotate(' + -this.rotation + 'rad) scale(0.79)';
    }
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  rotateCoins(itemPosition) {
    //this.turnRight()
    //this.turnRight()
    if (itemPosition !== 0) {
      let newAry = [];
      if (itemPosition == 1) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i == this.coinsAry.length - 1) {
            newAry.push(this.coinsAry[0]);
          } else {
            newAry.push(this.coinsAry[i + 1]);
          }
        }
        this.coinsAry = newAry;
        this.turnRight();
      } else if (itemPosition == 2) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i >= this.coinsAry.length - 2) {
            if (i == this.coinsAry.length - 2) {
              newAry.push(this.coinsAry[0]);
            } else {
              newAry.push(this.coinsAry[1]);
            }
          } else {
            newAry.push(this.coinsAry[i + 2]);
          }
        }
        this.coinsAry = newAry;
        this.turnRight();
        this.turnRight();
      } else if (itemPosition == 3) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i >= this.coinsAry.length - 3) {
            if (i == this.coinsAry.length - 3) {
              newAry.push(this.coinsAry[0]);
            } else if (i == this.coinsAry.length - 2) {
              newAry.push(this.coinsAry[1]);
            } else {
              newAry.push(this.coinsAry[2]);
            }
          } else {
            newAry.push(this.coinsAry[i + 3]);
          }
        }
        this.coinsAry = newAry;
        this.turnRight();
        this.turnRight();
        this.turnRight();
      } else if (itemPosition == 11) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i == 0) {
            newAry.push(this.coinsAry[this.coinsAry.length - 3]);
          } else if (i == 1) {
            newAry.push(this.coinsAry[this.coinsAry.length - 2]);
          } else if (i == 2) {
            newAry.push(this.coinsAry[this.coinsAry.length - 1]);
          } else {
            newAry.push(this.coinsAry[i - 3]);
          }
        }
        this.coinsAry = newAry;
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
      } else if (itemPosition == 12) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i == 0) {
            newAry.push(this.coinsAry[this.coinsAry.length - 2]);
          } else if (i == 1) {
            newAry.push(this.coinsAry[this.coinsAry.length - 1]);
          } else {
            newAry.push(this.coinsAry[i - 2]);
          }
        }
        this.coinsAry = newAry;
        this.turnLeft();
        this.turnLeft();
      } else if (itemPosition == 13) {
        for (let i = 0; i < this.coinsAry.length; i++) {
          if (i == 0) {
            newAry.push(this.coinsAry[this.coinsAry.length - 1]);
          } else {
            newAry.push(this.coinsAry[i - 1]);
          }
        }
        this.coinsAry = newAry;
        this.turnLeft();
      }
    }
  }

  coinsAry: any = [1000, 1010, 1020, 103, 104, 105, 106, 107, 108, 109, 110, 111, 1120, 1130];


  toggleIsBatPlacedOpen(): void {
    this.isBatPlacedOpen = true;
    setTimeout(() => {
      const batPlaced = document.getElementsByClassName('doranim');
      batPlaced[0].className += ' ' + 'opendoor';
    }, 0);

    setTimeout(() => {
      const batPlaced = document.getElementsByClassName('doranim');
      batPlaced[0].className = batPlaced[0].className.replace('opendoor', '');
    }, 3000);

    setTimeout(() => {
      this.isBatPlacedOpen = false;
      this.isWinnerDecided = true;

      const andarSecondSectionCss = document.getElementsByClassName('andar-second-section')[0];
      const andarSecondSectionCssMob = document.getElementsByClassName('andar-second-sectionMob')[0];
      if (andarSecondSectionCss) {
        andarSecondSectionCss.className += ' ' + 'win';

        setTimeout(() => {
          this.isWinnerDecided = false;
          andarSecondSectionCss.className = andarSecondSectionCss.className.replace('win', '');
          this.resetAllVariablesOfBet();
        }, 3000);
      }
      if (andarSecondSectionCssMob) {
        andarSecondSectionCssMob.className += ' ' + 'win';

        setTimeout(() => {
          this.isWinnerDecided = false;
          andarSecondSectionCssMob.className = andarSecondSectionCssMob.className.replace('win', '');
          this.resetAllVariablesOfBet();
        }, 3000);
      }
    }, 4000);
  }

  resetAllVariablesOfBet(): any {
    this.isBatPlacedOpen = false;
    this.isWinnerDecided = false;
  }

}
